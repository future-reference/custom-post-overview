<?php

/**
 * @wordpress-plugin
 * Plugin Name:       Custom Post Type - Overview
 * Plugin URI:        https://gitlab.com/future-reference/custom-post-overview
 * Description:       Wordpress Plugin to create a custom post type named overview
 * Version:           1.0.1
 * Author:            Future Reference
 * Author URI:        http://futurereference.co/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       custom-post-overview
 */

add_action('init', 'overview_register_post_types');

function overview_register_post_types()
{

    /* Register the Overview post type. */

    register_post_type(
        'overview',
        array(
            'description'         => '',
            'public'              => true,
            'publicly_queryable'  => true,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => true,
            'exclude_from_search' => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 20,
            'menu_icon'           => 'dashicons-media-default',
            'can_export'          => true,
            'delete_with_user'    => false,
            'hierarchical'        => false,
            'has_archive'         => false,
            'capability_type'     => 'post',

            /* The rewrite handles the URL structure. */
            'rewrite' => array(
                'slug'       => 'overview',
                'with_front' => false,
                'pages'      => true,
                'feeds'      => true,
                'ep_mask'    => EP_PERMALINK,
            ),

            /* What features the post type supports. */
            'supports' => array(
                'title',
                'editor',
                'revisions',
                'thumbnail'
            ),

            /* Labels used when displaying the posts. */
            'labels' => array(
                'name'               => __('Overview',                    'futurereference'),
                'singular_name'      => __('Overview',                     'futurereference'),
                'menu_name'          => __('Overview',                    'futurereference'),
                'name_admin_bar'     => __('Overview',                    'futurereference'),
                'add_new'            => __('Add New',                    'futurereference'),
                'add_new_item'       => __('Add New Overview',             'futurereference'),
                'edit_item'          => __('Edit Overview',                'futurereference'),
                'new_item'           => __('New Overview',                 'futurereference'),
                'view_item'          => __('View Overview',                'futurereference'),
                'search_items'       => __('Search Overviews',             'futurereference'),
                'not_found'          => __('No overviews found',           'futurereference'),
                'not_found_in_trash' => __('No overviews found in trash',  'futurereference'),
                'all_items'          => __('Overviews',                    'futurereference'),
            )
        )
    );
}
